package domain;

public enum Category {
    GRAPHIC_CARD,
    HARD_DISK,
    MEMORY,
    MOTHERBOARD  
    ;
}
