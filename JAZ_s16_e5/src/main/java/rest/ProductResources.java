package rest;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import domain.Comment;
import domain.Product;


@Path("/products")
@Stateless
public class ProductResources {

	@PersistenceContext
	EntityManager em;

//show all products
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getAll()
	{
		return em.createNamedQuery("product.all", Product.class).getResultList();
	}
	
//add new product
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response Add(Product product){
		em.persist(product);
		return Response.ok(product.getId()).build();
	}
	
//show a product with given id
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("id") int id){
		Product result = em.createNamedQuery("product.id", Product.class)
				.setParameter("product.id", id)
				.getSingleResult();
		if(result==null){
			return Response.status(404).build();
		}
		return Response.ok(result).build();
	}

//modify a product
	
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("id") int id, Product p){
		Product result = em.createNamedQuery("product.id", Product.class)
				.setParameter("product.id", id)
				.getSingleResult();
		if(result==null)
			return Response.status(404).build();
		result.setPrice(p.getPrice());
		result.setName(p.getName());
		result.setCategory(p.getCategory());
		em.persist(result);
		return Response.ok().build();
	}
	
//show a product with a given name
	
	@GET
	@Path("/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNamed(@PathParam("name") String name){
		List<Product> result = em.createNamedQuery("product.name", Product.class)
				.setParameter("code", "%" + name + "%")
				.getResultList();
		if(result==null){
			return Response.status(404).build();
		}
		return Response.ok(result).build();
	}

//show product with price from-to 

	@GET
	@Path("/{price1}-{price2}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPriced(@PathParam("price1") double price1, @PathParam("price2") double price2){
		List<Product> result = em.createNamedQuery("product.price", Product.class)
				.setParameter("price1", price1)
				.setParameter("price2", price2)
				.getResultList();
		if(result==null){
			return Response.status(404).build();
		}
		return Response.ok(result).build();
	}
	
//show products of ggiven category

		@GET
		@Path("/{category}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getCategory(@PathParam("category") String category){
			List<Product> result = em.createNamedQuery("product.category", Product.class)
					.setParameter("product.category", category)
					.getResultList();
			if(result==null){
				return Response.status(404).build();
			}
			return Response.ok(result).build();
		}
	
//add comments

	    @POST
	    @Path("/{id}/comments")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response addComment(@PathParam("id") int id, Comment comment) {
	       
	         Product result = em.createNamedQuery("product.id", Product.class)
	                    .setParameter("productId", id)
	                    .getSingleResult();
	        result.getComments().add(comment);
	        comment.setProduct(result);
	        em.persist(comment);

	        return Response.ok(comment).build();
	    }
	    
//delete comments  
	    
	    @DELETE
	    @Path("/{id}/comments/{commentId}")
	    public Response deleteComment(@PathParam("id") int id, @PathParam("commentId") int commentId) {
	        Product result = em.createNamedQuery("product.id", Product.class)
	                    .setParameter("productId", id)
	                    .getSingleResult();
	        if(result==null){
	            return Response.status(404).build();
	        }
	        List<Comment> comments = result.getComments();
	        for (int i = 0; i < comments.size(); i++) {
	            if (comments.get(i).getId()==(commentId)) {
	                comments.get(i).setProduct(null);
	                comments.remove(i);
	            }
	        }
	        em.merge(result);

	        return Response.ok().build();
	    }
	    
//show comments
	    
	    @GET
	    @Path("/{id}/comments")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response getComments(@PathParam("id") int id) {
	        Product result = em.createNamedQuery("product.id", Product.class)
	                    .setParameter("productId", id)
	                    .getSingleResult();
	        if(result==null){
	            return Response.status(404).build();
	        }
	        return Response.ok(result.getComments()).build();
	    }

}
